package itis.parsing;

import itis.parsing.annotations.FieldName;
import itis.parsing.annotations.MaxLength;
import itis.parsing.annotations.NotBlank;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ParkParsingServiceImpl implements ParkParsingService {

    @Override
    public Park parseParkData(String parkDatafilePath) throws ParkParsingException {

        Map<String,String> fieldValueMap= new HashMap<>();

        try {
            Constructor<Park> declaredConstructor = Park.class.getDeclaredConstructor();
            declaredConstructor.setAccessible(true);
            Park nextPark = declaredConstructor.newInstance();

            List<ParkParsingException.ParkValidationError> stackOfErrors= new ArrayList<>();

            //чтение файла
            BufferedReader bfr = new BufferedReader(new FileReader(parkDatafilePath));
            String line="";
            while ((line = bfr.readLine()) != null){
                if (line.equals("***")) continue;
                String[] splittedLine = line.split(":");
                String fieldName = splittedLine[0].substring(1,splittedLine[0].length()-2);
                String fieldValue = splittedLine[1];
                if (splittedLine[1].charAt(0)=='"'){
                    fieldValue = splittedLine[1].substring(1,splittedLine[1].length()-2);
                }
                fieldValueMap.put(fieldName,fieldValue);
            }
            bfr.close();


            // работа с полями
            Field[] declaredFields = Park.class.getDeclaredFields();
            for (String name: fieldValueMap.keySet()){
                for (Field field: declaredFields){

                    if (name.equals(field.getName())){
                        field.setAccessible(true);
                        field.set(nextPark,fieldValueMap.get(name));

                        if (field.isAnnotationPresent(FieldName.class)){
                            String srcFieldName = field.getAnnotation(FieldName.class).value();

                            Field srcField = null;
                            try {
                                srcField = nextPark.getClass().getField(srcFieldName);
                                field.set(nextPark,srcField.get(nextPark));

                            } catch (NoSuchFieldException | IllegalAccessException e) {
                                stackOfErrors.add(
                                        new ParkParsingException.ParkValidationError
                                        (name,"Для поля"+name+"не найдено поле-источник "+srcFieldName));
                            }


                        }
                        if (field.isAnnotationPresent(MaxLength.class)
                                && field.getType()==String.class
                                && fieldValueMap.get(name).length() > field.getAnnotation(MaxLength.class).value())
                        {
                            stackOfErrors.add(
                                    new ParkParsingException.ParkValidationError
                                    (name, "Длина поля " + name + "должна быть не больше " + field.getAnnotation(MaxLength.class).value()));
                        }
                        if (field.isAnnotationPresent(NotBlank.class)
                                && (fieldValueMap.get(name)!=null || !fieldValueMap.get(name).equals("")))
                        {
                            stackOfErrors.add(
                                    new ParkParsingException.ParkValidationError
                                    (name,"Поле "+name+" не может быть пустым" ));
                        }

                        if (!stackOfErrors.isEmpty()) {
                            throw new ParkParsingException("При парсинге возникли ошибки",stackOfErrors);
                        }

                    }
                }
            }
            return nextPark;
        } catch (IOException | IllegalAccessException | NoSuchMethodException | InvocationTargetException | InstantiationException e) {
            e.printStackTrace();
        }
        return null;
    }
}
